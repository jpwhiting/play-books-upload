#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

# First authorize
SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/books'
]

__license__ = 'GPL v3'
__copyright__ = '2021, Jeremy Whiting <jpwhiting@kde.org>'
__docformat__ = 'restructuredtext en'

import json
import os
import sys
import traceback
import time

from calibre.customize.ui import quick_metadata
from calibre.ebooks import DRMError
from calibre.ptempfile import cleanup
from calibre.utils.ipc.server import Server
from calibre.utils.ipc.job import ParallelJob

from calibre.utils.config import config_dir

pluginsdir = os.path.join(config_dir, "plugins")
maindir = os.path.join(pluginsdir, "play_books_upload")

def do_book_uploads(books_to_upload, cpus, notification=lambda x, y:x):
    '''
    Master job, to launch child jobs to upload books in this list of books
    '''
    server = Server(pool_size=cpus)

    # Queue all the jobs
    for book_id, title, ffile in books_to_upload:
        args = ['calibre_plugins.play_books_upload.jobs', 'do_upload_book',
                (book_id, title, ffile)]
        print("do_upload_book - book_id=%s, title=%s, filename=%s"
              % (book_id, title, ffile))
        job = ParallelJob('arbitrary', str(book_id), done=None, args=args)
        job._book_id = book_id
        job._title = title
        job._ffile = ffile
        server.add_job(job)
        print("do_upload_book - job started for file title=%s" % title)

    # This server is an arbitrary_n job, so there is a notifier available.
    # Set the % complete to a small number to avoid the 'unavailable' indicator
    notification(0.01, 'Uploading Books')

    # dequeue the job results as they arrive, saving the results
    total = len(books_to_upload)
    count = 0
    while True:
        job = server.changed_jobs_queue.get()
        # A job can 'change' when it is not finished, for example if it
        # produces a notification. Ignore these.
        job.update()
        if not job.is_finished:
            continue
        # A job really finished. Get the information.
        results = job.result
        book_id = job._book_id
        print('Job with id {} is {}'.format(book_id, job.result))
        count = count + 1
        notification(float(count) / total, 'Uploading Book')

        # Add this job's output to the current log
        print('-------------------------------')
        print('Logfile for book ID %d (%s)' % (book_id, job._title))

        print(job.details)

        if count >= total:
            # All done!
            break

    server.close()
    # return the map as the job result
    return 


def do_upload_book(book_id, title, ffile):
    '''
    Child job, to upload this specific book
    '''
    creds = None

    try:
        print("do_upload_book: ", book_id)

        os.chdir(maindir)
        sys.path.append(maindir)

        print("module path: {}".format(sys.path), flush=True)
        import googleapiclient.discovery
        import google_auth_oauthlib.flow
        import google.oauth2

        print("maindir is {0}".format(maindir))

        if os.path.exists('token.json'):
            with open('token.json', 'r') as token:
                print("Reading credentials from {0}".format(os.path.realpath(token.name)))
            creds = google.oauth2.credentials.Credentials.from_authorized_user_file('token.json')

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(google.auth.transport.requests.Request())
            else:
                flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file('client_secrets.json', SCOPES)
                creds = flow.run_local_server(port=0)

        with open('token.json', 'w') as token:
            token.write(creds.to_json())
            print("wrote credentials to {0}".format(os.path.realpath(token.name)))

        driveapi = googleapiclient.discovery.build('drive', 'v3', credentials=creds, static_discovery=False)
        files = driveapi.files()

        service = googleapiclient.discovery.build('books', 'v1', credentials=creds, static_discovery=False)
        cloudloading = service.cloudloading()

        volumes = service.volumes()
        useruploaded = volumes.useruploaded()

        print('book id: ' + str(book_id))
        print('uploading from: ' + ffile)

        gfile = files.create(body={"name":ffile}, media_body=ffile).execute()
        print(gfile)
        id = gfile["id"]
        print("Document id: {}\n".format(id))
        print("Title: {}\n".format(title))

        result = cloudloading.addBook(drive_document_id=id,name=title).execute()
        print(result)

        volumeId = result["volumeId"]

        processingState = 'RUNNING'

        # Wait until the processing finishes
        while processingState == 'RUNNING':
            # Check volume processing state 
            try:
                result = useruploaded.list(volumeId=volumeId).execute()
                if 'totalItems' in result:
                    print ("Current upload count: {}".format(result['totalItems']))

                if 'items' in result:
                    items = result['items']
                    thisitem = items[0]
                    userInfo = thisitem['userInfo']
                    userUploadedVolumeInfo = userInfo['userUploadedVolumeInfo']
                    processingState = userUploadedVolumeInfo['processingState']
                    print("Processing state: {}".format(processingState))
            except:
                print("Error getting volume metadata")

        # Now delete the file from google drive
        files.delete(fileId = id).execute()
        print("File deleted from google drive after import")

        service.close()

        driveapi.close()

        return
    except:
        print("Exception thrown {}".format(traceback.format_exc()))

