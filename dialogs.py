#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2021, Jeremy Whiting <jeremypwhiting@gmail.com>'
__docformat__ = 'restructuredtext en'

import os, traceback
from collections import OrderedDict
try:
    from PyQt5.Qt import QProgressDialog, QTimer
except ImportError:
    from PyQt4.Qt import QProgressDialog, QTimer

from calibre.gui2 import warning_dialog

try:
    load_translations()
except NameError:
    pass # load_translations() added in calibre 1.9

class QueueProgressDialog(QProgressDialog):

    def __init__(self, gui, book_ids, tdir, queue, db):
        QProgressDialog.__init__(self, _('Working...'), _('Cancel'), 0, len(book_ids), gui)
        self.setWindowTitle(_('Queueing books for uploading'))
        self.setMinimumWidth(500)
        self.book_ids = book_ids
        self.queue = queue
        self.db = db
        self.gui = gui
        self.tdir = tdir
        self.i = 0
        self.books_to_upload = []
        self.bad = OrderedDict()
        self.warnings = []

        QTimer.singleShot(0, self.do_book)
        self.exec_()

    def do_book(self):
        book_id = self.book_ids[self.i]
        self.i += 1
        
        db = self.db.new_api

        print('book id: ' + str(book_id), flush=True)
        # Get the current metadata for this book from the db
        mi = db.get_metadata(book_id, get_cover=False)
        fmts = db.formats(book_id)
        print('book has formats ' + ' '.join(fmts))
        ffile = os.path.join(self.tdir, '%d.%s'%(book_id, 'epub'))
        if not fmts:
            print("Book has no formats to upload")
            return
        if 'epub' in fmts:
            # book has epub type, so upload it
            with open(ffile, "w+b") as f:
                self.db.copy_format_to(book_id, 'epub', f, index_is_id=True)
            print('epub of book is at ' + ffile)
        elif 'EPUB' in fmts:
            # book has EPUB type, so upload it
            with open(ffile, "w+b") as f:
                self.db.copy_format_to(book_id, 'EPUB', f, index_is_id=True)
            print('EPUB of book is at ' + ffile)
        else:
            self.bad[book_id] = _('No epub format found')
            # book has no ebub format, so show an error message
            print('no epub of book, not uploading anything')
            return

        try:
            title = self.db.title(book_id, index_is_id=True)
            done = False

            if not done:
                self.books_to_upload.append((book_id, title, ffile))
                done = True
        except:
            traceback.print_exc()
            self.bad[book_id] = traceback.format_exc()

        self.setValue(self.i)
        if self.i >= len(self.book_ids):
            return self.do_queue()
        else:
            QTimer.singleShot(0, self.do_book)

    def do_queue(self):
        self.hide()
        res = []
        distinct_problem_ids = {}
        msg = ''
        for book_id, warning in self.warnings:
            if book_id not in distinct_problem_ids:
                distinct_problem_ids[book_id] = True
            title = self.db.title(book_id, True)
            res.append('%s (%s)'%(title, warning))
        if len(self.bad):
            for book_id, error in self.bad.items():
                if book_id not in distinct_problem_ids:
                    distinct_problem_ids[book_id] = True
                title = self.db.title(book_id, True)
                res.append('%s (%s)'%(title, error))
        msg = msg + '\n'.join(res)
        if len(res) > 0:
            summary_msg = _('Could not upload %d of %d books, for reasons shown in details below.')
            warning_dialog(self.gui, _('Upload warnings'),
                summary_msg % (len(distinct_problem_ids), len(self.book_ids)), msg).exec_()
        self.gui = None
        # Queue a job to process these books
        self.queue(self.tdir, self.books_to_upload)
