This calibre plugin is to upload books to google play to
read the .epub files in Play Books on android.

It does 2 things after authenticating currently.

1. Upload the epub of selected books to google drive and get
   the file's drive id.
2. Add the book to play books library so they appear in 
   https://play.google.com/books/uploads

TODO:    
Probably should delete the .epub files from google drive after
adding them to play books to not clutter up user's google drive
main folders.

To use:

Getting dependencies:
    pip install --upgrade -r requirements.txt -t .

    then add to calibre with:
    calibre-customize -b .

    To test select any book and click the button on the toolbar.
