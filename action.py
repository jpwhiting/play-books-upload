#!/usr/bin/env python3
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2019, Jeremy Whiting <jpwhiting@kde.org>'
__docformat__ = 'restructuredtext en'

if False:
    # This is here to keep my python error checker from complaining about
    # the builtin functions that will be defined by the plugin loading system
    # You do not need this code in your plugins
    get_icons = get_resources = None

import os

# The class that all interface action plugins must inherit from
from calibre.gui2.actions import InterfaceAction
from calibre_plugins.play_books_upload.main import UploadDialog
from calibre.utils.config import config_dir
from calibre.utils.zipfile import ZipFile
from calibre.ptempfile import PersistentTemporaryDirectory, remove_dir

# Import from this plugin
from calibre_plugins.play_books_upload import PLUGIN_NAME, PLUGIN_CAPTION
from calibre_plugins.play_books_upload.dialogs import QueueProgressDialog

class PlayUploadAction(InterfaceAction):

    name = 'Upload to Play Books'

    # Declare the main action associated with this plugin
    # The keyboard shortcut can be None if you dont want to use a keyboard
    # shortcut. Remember that currently calibre has no central management for
    # keyboard shortcuts, so try to use an unusual/unused shortcut.
    action_spec = ('Upload to Play Books', None,
            _('Upload selected books to google drive then add books to play books'), ())
    action_type = 'current'
    dont_add_to = frozenset(['context-menu-device'])
    actual_plugin       = 'calibre_plugins.play_books_upload.action:PlayUploadAction'
    maindir = ''

    def genesis(self):
        self.is_library_selected = True
        # This method is called once per plugin, do initial setup here

        # Set the icon for this interface action
        # The get_icons function is a builtin function defined for all your
        # plugin code. It loads icons from the plugin zip file. It returns
        # QIcon objects, if you want the actual data, use the analogous
        # get_resources builtin function.
        #
        # Note that if you are loading more than one icon, for performance, you
        # should pass a list of names to get_icons. In this case, get_icons
        # will return a dictionary mapping names to QIcons. Names that
        # are not found in the zip file will result in null QIcons.
        icon = get_icons('images/upload.png')

        # The qaction is automatically created from the action_spec defined
        # above
        self.qaction.setIcon(icon)
        self.qaction.triggered.connect(self.upload_books)

        self.pluginsdir = os.path.join(config_dir,"plugins")
        if not os.path.exists(self.pluginsdir):
            os.mkdir(self.pluginsdir)
        self.maindir = os.path.join(self.pluginsdir,"play_books_upload")
        print("Set maindir to {0}".format(self.maindir))

        # Now extract all our plugin contents so we can import them easily later
        with ZipFile(self.plugin_path) as zf:
            zf.extractall(self.maindir)
            
        self.plugin_callback = None


    def upload_books(self):
        if not self.is_library_selected:
            return
        rows = self.gui.library_view.selectionModel().selectedRows()
        if not rows or len(rows) == 0 :
            return
        book_ids = self.gui.library_view.get_selected_ids()
        
        tdir = PersistentTemporaryDirectory('_play_booksupload', prefix='')
        
        QueueProgressDialog(self.gui, book_ids, tdir, self._queue_job, self.gui.current_db)

    def _queue_job(self, tdir, books_to_upload):
        if not books_to_upload:
            return
            
        func = 'arbitrary_n'
        cpus = self.gui.job_manager.server.pool_size
        args = ['calibre_plugins.play_books_upload.jobs', 'do_book_uploads',
                (books_to_upload, cpus)]
        desc = _('Upload books')
        job = self.gui.job_manager.run_job(
                self.Dispatcher(self._upload_books_completed), func, args=args, description=desc)
        job.tdir = tdir
        job.plugin_callback = self.plugin_callback
        self.plugin_callback = None
        
    def _upload_books_completed(self, job):
        # Remove the temp folder since we are done with it
        if job.tdir:
            remove_dir(job.tdir)
            
        if job.failed:
            return self.gui.job_exception(job, dialog_title=_('Failed to upload book(s)'))
        self.gui.status_bar.show_message(_('Book uploads completed'), 3000)
        
    def apply_settings(self):
        from calibre_plugins.play_books_upload.config import prefs
        # In an actual non trivial plugin, you would probably need to
        # do something based on the settings in prefs
        prefs

    def location_selected(self, loc):
        self.is_library_selected = loc == 'library'

